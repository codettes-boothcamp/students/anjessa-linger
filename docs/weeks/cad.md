# Cad means computer aided design
# Tinkercad 
We had to make a 3d model of our final project and i made it in tinkercad and fusion 360.   
My final project is a portable beamer I made two versions a cube and a beam.

1.  select an object    
. ![Screenshot](../img/cad/cad1.jpg)
2.  select a cylinder and select hole   
  ![Screenshot](../img/cad/cad2.jpg) 
3.  ctrl+g this will group your design   
4.  export as and stl file   
![Screenshot](../img/cad/cad3.jpg)


# Fusion 360
*  select a pan you want to work on then click r for rectangle and a rectangle will appear   
![Screenshot](../img/cad/fusion1.jpg) 
*  select e wich will extrude your design you can decide how much you want to extrude   
![Screenshot](../img/cad/fusion2.jpg)
*  type H for hole select a face point and decide how deep you want your hole to be you can drag the end or type it in the hole control section  
![Screenshot](../img/cad/fusion3.jpg)
*  to export a file go to file click on export and export it as an stl file  
![Screenshot](../img/cad/fusion4.jpg )