## Electronics production
## Week 1
install kicad 
install flatcam 
import the library (mod pretty)
1. open kicad and select  new project    
![Screenshot](../img/ed/ed1.png)

2.  import symbol 

  *   go to preferences
  *   manage libs 
  *   browse lib 
  *   fab lib   
  ![Screenshot](../img/ed/ed3.png)  
  the components we will be using are:
  ![Screenshot](../img/ed/ed27.jpg)
    
Atmega328p-Au

 ![Screenshot](../img/ed/ed28.jpg)
 3. place the components using the fab library 
 
 ![Screenshot](../img/ed/ed4.png)

 ![Screenshot](../img/ed/ed6.png)
 4. annotate to see if you labeled your symbols correctly (1,2,3 etc instead of 1,1)  
if it just closed everything is labeled correctly .    

 ![Screenshot](../img/ed/ed7.png)
 
   assign footprints 
( *  manage footprint libraries 
  *  go to the mode file )
  
  
 ![Screenshot](../img/ed/ed9.jpg) 
 
 ![Screenshot](../img/ed/ed10.jpg)  
 
 ![Screenshot](../img/ed/ed11.jpg)  
 

 5. Generate net list     
  ![Screenshot](../img/ed/ed13.jpg)  
6. Open Pcb new
( 
*Select net
* Search for the folder where the netlist is
*Read current netlist)  

  ![Screenshot](../img/ed/ed14.jpg)  
  
7. Select setup and select design rules 
8. Rearrange the components and trace the routes 
9. Add dimensions

![Screenshot](../img/ed/ed15.jpg)  

10.  Select file and select plot once you've done that make sure all these things are checked
(* mark F.cut
 * use auxillary as origin 
 *  select output directory) 
 
 ![Screenshot](../img/ed/ed24.jpg)  
 
 after you've plotted generate your gerber files   
11. After you created your gerber files open flatcam 
file - open gerber

![Screenshot](../img/ed/ed16.jpg)  

go to options and select mm  
these are the flatcam parameters 
  
![Screenshot](../img/ed/ed29.jpg)  


12. Select your grb files change the parameters and then click generate this will be demonstated in the pictures.     

![Screenshot](../img/ed/ed17.jpg)  
![Screenshot](../img/ed/ed18.jpg)  
![Screenshot](../img/ed/ed19.jpg)  
![Screenshot](../img/ed/ed20.jpg)  
![Screenshot](../img/ed/ed23.jpg)  
![Screenshot](../img/ed/ed25.jpg)  
![Screenshot](../img/ed/ed26.png)  
13. Export G-code   


## Week 2 Milling and soldering the board 
### cnc milling
- place the copper plate and secure it with double sided tape
- import your cutout and frontcut files
- Check the following parameters:
- Cnc Mill Probe Z physical
- Gnd Pin Connected
- Magnet on pcb
- Probe Z
- Test pin vcc to pcb (twice)

### soldering 

