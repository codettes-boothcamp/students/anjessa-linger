# Embedded Programming
### Assignment
* Practicing with the Arduino Uno kit using components to make a circuit
* Installing Arduino-1.8.9 for windows
* Programming basics in the Arduino IDE

### Learning outcomes
* Working with The Arduino Uno Board connecting it with our Laptop using a USB cable and giving the commands by coding it in the Arduino IDE
* Knowing the Breadboard and all other components in the Arduino Kit and checking its specs online if needed by typing the Serial Number
* Understanding the Electrical Current Flow

### Arduino uno  
the arduino uno is a opensource micro controller based on the ATmega328P
We started with the arduino starter kit which included:
(i only listed the one's we used)

**The arduino uno**

![Screenshot](../img/IMG_1403__1_.jpg)  

**Breadboard**
a breadboard allows you to build a circuit without the need of soldering

![Screenshot](../img/breadboard.png) 

**Resistor** 
a resistor is a passive component that implements electrical resistance as a circuit element. 
The resistors have different colors and these colors have meaning  which you can see on the Resistance color code table below. 
 
![Screenshot](../img/IMG_1423__1_.jpg)  


**LDR**
A light dependent resistor or photoresistor  is a passive component that decreases resistance with respect to receiving luminosity (light) on the component's sensitive surface.
 
**Potentiometer**
A potentiometer is a three-terminal resistor with a sliding or rotating contact that forms an adjustable voltage divider.

**Led**
A led is a light emitting diode. A diode has an anode and a cathode, the anode is the positive side (the long leg) and the cathode is the negative side (short leg)

![Screenshot](../img/led.jpg)

**DHT11**
The DHT11 is a basic, ultra  digital temperature and humidity sensor.

![Screenshot](../img/dht11.jpg)

  

After installing arduino IDE and opening the app this is what it looks like. 

![Screenshot](../img/2019-11-20__1_.jpg)  
To connect your computer to the arduino uno you have to do two things. First connecting the arduino to the computer using the usb cable   

and the second you can see on the screenshot below.   
![Screenshot](../img/EFCB2300-4872-457C-9B15-D95DE9246048__2_.jpg)

![Screenshot](../img/2019-11-20__2_.jpg)  
You should also check which board you have if it's not arduino uno you have to select arduino uno You do this by tools->Board->arduino uno.  

![Screenshot](../img/2019-11-20__3_.jpg)   
Open the Blink example by going to File-->Examples-->01.Basics-->Blink  

![Screenshot](../img/blink.jpg)


While coding you can verify your code by using the verify option.  
![Screenshot](../img/verify.jpg)

To use this code you have to put a 220 ohm resistor in front of the LED.
This is the code.

 
![Screenshot](../img/IMG_1432__1_.jpg)  


![Screenshot](../img/IMG_1500__1_.jpg)  

![Screenshot](../img/IMG_1475__1___1_.jpg) 

this is the blink code.  

```
int switchState = 0;
void setup() {
  // initialize digital pin 13 as an output.
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(2, INPUT);
  
}

// the loop function runs over and over again forever
void loop() {
  switchState= digitalRead(2);//a comment because im bored
  digitalWrite(13, HIGH);
  digitalWrite(12, HIGH);
  digitalWrite(11, HIGH);// turn the LED on (HIGH is the voltage level)
  delay(500);              // wait for a second
   digitalWrite(13, LOW);
   digitalWrite(12, LOW);
   digitalWrite(11, LOW);// turn the LED off by making the voltage LOW
  delay(500);              // wait for a second
  
}

```

To define a pin you have to go to void setup and write 
``` pinMode(whatever pin you want to define, input or output)```
What you put in the setup only runs once.
We used three leds and they were defined in pinmode 13,12 and 11.

In the loop the code runs "forever"
With digitalWrite you're explaining when you want the leds on and off (you van use high,low or 0,1)
With the delay you can set how long you want the lamp to stay off.
With this code we want the leds to blink together so they're either all on or all off.




This is the spaceship interface code  

```
int switchState = 0;
void setup() {
  // initialize digital pin LED_BUILTIN as an output-/inMode(12, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(2, INPUT);
  
}

// the loop function runs over and over again forever
void loop() {
  switchState = digitalRead(2);
  // this is a comment
  if (switchState==LOW){
    digitalWrite(3, HIGH);   // turn the LED on (HIGH is the voltage level)
    digitalWrite(4, HIGH);   // turn the LED on (HIGH is the voltage level)
    digitalWrite(5, HIGH);   // turn the LED on (HIGH is the voltage level)
  }
  else{// wait for a second
    digitalWrite (3,LOW);    // turn the LED off by making the voltage LOW
    digitalWrite(4, LOW);    // turn the LED off by making the voltage LOW
    digitalWrite(5, HIGH);    // turn the LED off by making the voltage LOW
    delay(250);                       // wait for a second */
    digitalWrite(4, HIGH);
    digitalWrite(5, LOW);
    digitalWrite(3, LOW);
    delay(250);
    digitalWrite(4, LOW);
    digitalWrite(5, HIGH);
    digitalWrite(3, LOW);
    delay(250);
  
    digitalWrite(4, LOW);
    digitalWrite(5, LOW);
    digitalWrite(3, HIGH);
    delay(250);
  }
} 
 
 
``` 

the goal with the spaceship interface is to blink 3 leds in sequence using a switch.
we defined pinmode 2 as input because we want to detect the state.(on and off) 

```
int switchState = 0;
void setup() {
  // initialize digital pin LED_BUILTIN as an output-/inMode(12, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(2, INPUT);  

}

void ledsOff(){
  digitalWrite(3,LOW); 
  digitalWrite(4,LOW);
  digitalWrite(5,LOW);
}
void runningLeds(){
  ledsOff();
  digitalWrite(5, HIGH);    // turn the LED off by making the voltage LOW
  delay(250);
  
  ledsOff();
  digitalWrite(4, HIGH);
  delay(250);
  
  ledsOff();
  digitalWrite(3, HIGH);
  delay(250);
}
// the loop function runs over and over again forever
void loop() {
  switchState = digitalRead(2);
  // this is a comment
  if (switchState==LOW){
    digitalWrite(3, HIGH);   // turn the LED on (HIGH is the voltage level)
    digitalWrite(4, HIGH);   // turn the LED on (HIGH is the voltage level)
    digitalWrite(5, HIGH);   // turn the LED on (HIGH is the voltage level)
  }
  else{// wait for a second
    runningLeds();
  }
}

```
We used procedural programming to make the code more efficient

## SERIAL MONETERING
SERIAL COMMUNICATION
The serial monitor is used so you can see whats happening in the arduino on your screen.

```
void setup() {
// setup your serial
Serial.begin(9600);
Serial.println("hello codettes");
}

void loop() {
// Send a message to your serial port/monitor
Serial.println(millis());
delay(2000);
}
```

```
int analogPin = A0;
int lightState = 0;
int minimumLight = 184;
int maximumLight = 1022;
int lightPc = 0;
int switchState = 0;

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  Serial.print("hello");
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(2, INPUT);
}

 void ledsOff(){
  digitalWrite(3, LOW);    // turn the LED off by making the voltage LOW
  digitalWrite(4, LOW);    // turn the LED off by making the voltage LOW
  digitalWrite(5, LOW);    // turn the LED off by making the voltage LOW

  }

  void runningLeds(){
  ledsOff();
  digitalWrite(3, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay (1000);

  digitalWrite(4, HIGH);    // turn the LED off by making the voltage LOW
  delay (1000);
  
  digitalWrite(5, HIGH);    // turn the LED off by making the voltage LOW
  delay (1000);  

  }

// the loop routine runs over and over again forever:

void loop() {
  // read the input on analog pin 0:
  lightState = analogRead(A0);
  Serial.println(lightState);
  lightPc = (lightState - minimumLight)*100L/(maximumLight-minimumLight);
  Serial.println(lightPc);
  
  ledsOff();
  if (lightPc>=30){digitalWrite(3,1);}
  if (lightPc>=60){digitalWrite(4,1);}
  if (lightPc>=90){digitalWrite(5,1);}
 
  delay (1000);


    }
    
       // delay in between reads for stability
 ```

      




```
  void dimLed(int ledPin, int dutyCycle){
  int cycleTime = 10 ;//in milliseconds
  digitalWrite(ledPin, 1);
  delay((dutyCycle*cycleTime)/100);
  digitalWrite(ledPin,0);
  delay((100-dutyCycle)* cycleTime/100);
}

void loop() {

  dimLed(5,100);
  
  ```
  
  
  
  
  
  
 ```
  void dimLed(int ledPin, int dutyCycle){
  int cycleTime = 10 ;//in milliseconds
  digitalWrite(ledPin, 1);
  delay((dutyCycle*cycleTime)/100);
  digitalWrite(ledPin,0);
  delay((100-dutyCycle)* cycleTime/100);
}

void loop() {
  //dimLed(4,50);
  analogWrite(4,100);
for(int i=0;i<=255;i++){
  analogWrite(5,i);
  delay(10);
  
```
  
   
   
   
   
   
   
   
    **potentiometer/pwm**
    
    pwm mean pulse width modulation 
    
    
   
  
```
   int analogPin = A0;
int lightState = 0;
int minimumLight = 184;
int maximumLight = 1022;
int lightPc = 0;
int switchState = 0;
int potValue=0;
// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  Serial.print("hello");
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(2, INPUT);
}

 void ledsOff(){
  digitalWrite(3, LOW);    // turn the LED off by making the voltage LOW
  digitalWrite(4, LOW);    // turn the LED off by making the voltage LOW
  digitalWrite(5, LOW);    // turn the LED off by making the voltage LOW

  }

void runningLeds(){
  ledsOff();
  digitalWrite(3, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay (250);
  ledsOff();
  digitalWrite(4, HIGH);    // turn the LED off by making the voltage LOW
  delay (250);
  ledsOff();
  digitalWrite(5, HIGH);    // turn the LED off by making the voltage LOW
  delay (250); 

  }

// the loop routine runs over and over again forever:

void dimLed(int ledPin, int dutyCycle){
  int cycleTime = 10 ;//in milliseconds
  digitalWrite(ledPin, 1);
  delay((dutyCycle*cycleTime)/100);
  digitalWrite(ledPin,0);
  delay((100-dutyCycle)* cycleTime/100);
}

void loop() {
  potValue= analogRead(A0);
  //dimLed(4,50);
  analogWrite(3,potValue/4);
  analogWrite(5,potValue/4);
//  for(int i=0;i<=255;i++){
//    //analogWrite(5,i);
//    delay(10);
//  }
  Serial.println(potValue);
  delay(50);
}

```
![Screenshot](../img/pwm.jpg) 

## sensors
the DHT is digital temperature and humidity sensor.

![Screenshot](../img/dhtjpg.jpg) 


before you can use the dht you have to install the library first. 
![Screenshot](../img/dhtard.jpg) 





```
int pinDHT11 = 2;
SimpleDHT11 dht11(pinDHT11);

void setup() {
  Serial.begin(115200);
}

void loop() {
  // start working...
  Serial.println("=================================");
  Serial.println("Sample DHT11...");
  
  // read without samples.
  byte temperature = 0;
  byte humidity = 0;
  int err = SimpleDHTErrSuccess;
  if ((err = dht11.read(&temperature, &humidity, NULL)) != SimpleDHTErrSuccess) {
    Serial.print("Read DHT11 failed, err="); Serial.println(err);delay(1000);
    return;
  }
  
  Serial.print("Sample OK: ");
  Serial.print((int)temperature); Serial.print(" *C, "); 
  Serial.print((int)humidity); Serial.println(" H");
  
  // DHT11 sampling rate is 1HZ.
  delay(1500);
}


```

## motor control 
for motor control we used tinkercad. you can 3d design and build circuits 
 a dc motor is a direct current motor </br>

![Screenshot](../img/Screenshot__49_.jpg) </br>


![Screenshot](../img/Screenshot__56_.jpg) </br>

there is also a potentiomotor you can see on the volt motor </br>
![Screenshot](../img/Screenshot__55_.jpg)

```
const int pwm = 3;
const int in_1= 8;
const int in_2= 9;
void setup()
{
  pinMode(in_1, OUTPUT);
  pinMode(in_2, OUTPUT);
  pinMode(3,OUTPUT);
  Serial.begin(9600);
}

void loop()
{
 int duty = (analogRead(A0)-512)/2;
  Serial.println(duty);
  
  analogWrite(pwm,abs(duty));
  if (duty>0){
     //TURN CW
  digitalWrite(in_1, LOW);
  digitalWrite(in_2, HIGH);
  }
  if (duty<0){
     //TURN CW
  digitalWrite(in_1, HIGH);
  digitalWrite(in_2, LOW);
  }
  if ( duty==0){ 
  digitalWrite(in_1, LOW);
  digitalWrite(in_2, HIGH);
  }
}
```
servo motor
 Motor control Servo 
 ![Screenshot](../img/Screenshot__59_.jpg)
```
#include <Servo.h>
Servo myServo;
int const potPin = A0;
int potVal;
int angle;

void setup()
{
  myServo.attach(9);
  Serial.begin(9600);
  

}

void loop()
{
  potVal= analogRead(potPin);
  Serial.print("potVal: ");
  Serial.print(potVal);
  angle= map(potVal,0,1023,0,179);
  Serial.print(", angle:");
  Serial.println( angle);
  myServo.write(angle);
  delay(15);
  
}
```
**stepper motor**
this part is on shewishkas page.

[for the basics of electronics click here](https://www.makerspaces.com/basic-electronics/)

[you can download the arduino ide here](https://www.arduino.cc/en/main/software) 

[Tinkercad]( https://www.tinkercad.com/)   

## LCD keypad shield 
```
// You can have up to 10 menu items in the menuItems[] array below without having to change the base programming at all. Name them however you'd like. Beyond 10 items, you will have to add additional "cases" in the switch/case
// section of the operateMainMenu() function below. You will also have to add additional void functions (i.e. menuItem11, menuItem12, etc.) to the program.
String menuItems[] = {"Run Feeder", "Wait Time", "Feed Time", "Feed Now", "Save Settings", "Hi Mom!"};

// Navigation button variables
int readKey;
int savedDistance = 0;

// Menu control variables
int menuPage = 0;
int maxMenuPages = round(((sizeof(menuItems) / sizeof(String)) / 2) + .5);
int cursorPosition = 0;

int wait_time_seconds = 0; // seconds
int wait_time_minutes = 0; // minutes
int wait_time_hours   = 8; // hours
int feed_time = 5; // seconds
int redraw = 0;
int defaultMenuItem1 = 1;
int pos = 0;
int servoPin = 2;
int pulse = 1500;

// Creates 3 custom characters for the menu display
byte downArrow[8] = {
  0b00100, //   *
  0b00100, //   *
  0b00100, //   *
  0b00100, //   *
  0b00100, //   *
  0b10101, // * * *
  0b01110, //  ***
  0b00100  //   *
};

byte upArrow[8] = {
  0b00100, //   *
  0b01110, //  ***
  0b10101, // * * *
  0b00100, //   *
  0b00100, //   *
  0b00100, //   *
  0b00100, //   *
  0b00100  //   *
};

byte menuCursor[8] = {
  B01000, //  *
  B00100, //   *
  B00010, //    *
  B00001, //     *
  B00010, //    *
  B00100, //   *
  B01000, //  *
  B00000  //
};

#include <Wire.h>
#include <LiquidCrystal.h>
#include <Servo.h>
#include <EEPROM.h>

// Setting the LCD shields pins
LiquidCrystal lcd(8, 9, 4, 5, 6, 7);
Servo myservo;

void setup() {

  // Initializes serial communication
  Serial.begin(9600);

  myservo.attach(2);
  myservo.write(78); // STOP SERVO
  // myservo.writeMicroseconds(1500);

  // Initializes and clears the LCD screen
  lcd.begin(16, 2);
  lcd.clear();

  // Creates the byte for the 3 custom characters
  lcd.createChar(0, menuCursor);
  lcd.createChar(1, upArrow);
  lcd.createChar(2, downArrow);

  // Load EPROM
  int available_saved_value = EEPROMReadInt(1);
  if (available_saved_value == 1252) { 
    wait_time_seconds = EEPROMReadInt(3); // seconds
    wait_time_minutes = EEPROMReadInt(5); // minutes
    wait_time_hours   = EEPROMReadInt(7); // hours
    feed_time         = EEPROMReadInt(9); // seconds
  }
}

void loop() {
  mainMenuDraw();
  drawCursor();
  operateMainMenu();
  // menuItem1();
}

// This function will generate the 2 menu items that can fit on the screen. They will change as you scroll through your menu. Up and down arrows will indicate your current menu position.
void mainMenuDraw() {
  Serial.print(menuPage);
  lcd.clear();
  lcd.setCursor(1, 0);
  lcd.print(menuItems[menuPage]);
  lcd.setCursor(1, 1);
  lcd.print(menuItems[menuPage + 1]);
  if (menuPage == 0) {
    lcd.setCursor(15, 1);
    lcd.write(byte(2));
  } else if (menuPage > 0 and menuPage < maxMenuPages) {
    lcd.setCursor(15, 1);
    lcd.write(byte(2));
    lcd.setCursor(15, 0);
    lcd.write(byte(1));
  } else if (menuPage == maxMenuPages) {
    lcd.setCursor(15, 0);
    lcd.write(byte(1));
  }
}

// When called, this function will erase the current cursor and redraw it based on the cursorPosition and menuPage variables.
void drawCursor() {
  for (int x = 0; x < 2; x++) {     // Erases current cursor
    lcd.setCursor(0, x);
    lcd.print(" ");
  }

  // The menu is set up to be progressive (menuPage 0 = Item 1 & Item 2, menuPage 1 = Item 2 & Item 3, menuPage 2 = Item 3 & Item 4), so
  // in order to determine where the cursor should be you need to see if you are at an odd or even menu page and an odd or even cursor position.
  if (menuPage % 2 == 0) {
    if (cursorPosition % 2 == 0) {  // If the menu page is even and the cursor position is even that means the cursor should be on line 1
      lcd.setCursor(0, 0);
      lcd.write(byte(0));
    }
    if (cursorPosition % 2 != 0) {  // If the menu page is even and the cursor position is odd that means the cursor should be on line 2
      lcd.setCursor(0, 1);
      lcd.write(byte(0));
    }
  }
  if (menuPage % 2 != 0) {
    if (cursorPosition % 2 == 0) {  // If the menu page is odd and the cursor position is even that means the cursor should be on line 2
      lcd.setCursor(0, 1);
      lcd.write(byte(0));
    }
    if (cursorPosition % 2 != 0) {  // If the menu page is odd and the cursor position is odd that means the cursor should be on line 1
      lcd.setCursor(0, 0);
      lcd.write(byte(0));
    }
  }
}


void operateMainMenu() {

  int activeButton = 0;

  if (defaultMenuItem1 == 1) {
    menuItem1();
    defaultMenuItem1 = 0;
    activeButton = 1;
  }
  
  while (activeButton == 0) {
    int button;
    readKey = analogRead(0);
    if (readKey < 790) {
      delay(100);
      readKey = analogRead(0);
    }
    button = evaluateButton(readKey);
    switch (button) {
      case 0: // When button returns as 0 there is no action taken
        break;
      case 1:  // This case will execute if the "forward" button is pressed
        button = 0;
        switch (cursorPosition) { // The case that is selected here is dependent on which menu page you are on and where the cursor is.
          case 0:
            menuItem1();
            break;
          case 1:
            menuItem2();
            break;
          case 2:
            menuItem3();
            break;
          case 3:
            menuItem4();
            break;
          case 4:
            menuItem5();
            break;
          case 5:
            menuItem6();
            break;
          case 6:
            menuItem7();
            break;
          case 7:
            menuItem8();
            break;
          case 8:
            menuItem9();
            break;
          case 9:
            menuItem10();
            break;
        }
        activeButton = 1;
        mainMenuDraw();
        drawCursor();
        break;
      case 2:
        button = 0;
        if (menuPage == 0) {
          cursorPosition = cursorPosition - 1;
          cursorPosition = constrain(cursorPosition, 0, ((sizeof(menuItems) / sizeof(String)) - 1));
        }
        if (menuPage % 2 == 0 and cursorPosition % 2 == 0) {
          menuPage = menuPage - 1;
          menuPage = constrain(menuPage, 0, maxMenuPages);
        }

        if (menuPage % 2 != 0 and cursorPosition % 2 != 0) {
          menuPage = menuPage - 1;
          menuPage = constrain(menuPage, 0, maxMenuPages);
        }

        cursorPosition = cursorPosition - 1;
        cursorPosition = constrain(cursorPosition, 0, ((sizeof(menuItems) / sizeof(String)) - 1));

        mainMenuDraw();
        drawCursor();
        activeButton = 1;
        break;
      case 3:
        button = 0;
        if (menuPage % 2 == 0 and cursorPosition % 2 != 0) {
          menuPage = menuPage + 1;
          menuPage = constrain(menuPage, 0, maxMenuPages);
        }

        if (menuPage % 2 != 0 and cursorPosition % 2 == 0) {
          menuPage = menuPage + 1;
          menuPage = constrain(menuPage, 0, maxMenuPages);
        }

        cursorPosition = cursorPosition + 1;
        cursorPosition = constrain(cursorPosition, 0, ((sizeof(menuItems) / sizeof(String)) - 1));
        mainMenuDraw();
        drawCursor();
        activeButton = 1;
        break;
    }
  }
}

// This function is called whenever a button press is evaluated. The LCD shield works by observing a voltage drop across the buttons all hooked up to A0.
int evaluateButton(int x) {
  int result = 0;
  if (x < 50) {
    result = 1; // right
  } else if (x < 195) {
    result = 2; // up
  } else if (x < 380) {
    result = 3; // down
  } else if (x < 790) {
    result = 4; // left
  }
  return result;
}

// If there are common usage instructions on more than 1 of your menu items you can call this function from the sub
// menus to make things a little more simplified. If you don't have common instructions or verbage on multiple menus
// I would just delete this void. You must also delete the drawInstructions()function calls from your sub menu functions.
void drawInstructions() {
  lcd.setCursor(0, 1); // Set cursor to the bottom line
  lcd.print("Use ");
  lcd.write(byte(1)); // Up arrow
  lcd.print("/");
  lcd.write(byte(2)); // Down arrow
  lcd.print(" buttons");
}

void menuItem1() { // Function executes when you select the 2nd item from main menu
  int activeButton = 0;
  int redraw = 1;
  int period = 0;
  int counter = 0;
  int time_to_feed_hours   = wait_time_hours;
  int time_to_feed_minutes = wait_time_minutes;
  int time_to_feed_seconds = wait_time_seconds;

  while (activeButton == 0) {

    if (redraw == 1) {
        lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print("Feeding In: ");
        lcd.setCursor(0, 1);
        if (time_to_feed_hours < 10) lcd.print("0");
        lcd.print(time_to_feed_hours);
        lcd.print(":");
        if (time_to_feed_minutes < 10) lcd.print("0");
        lcd.print(time_to_feed_minutes);
        lcd.print(":");
        if (time_to_feed_seconds < 10) lcd.print("0");
        lcd.print(time_to_feed_seconds);

        // lcd.setCursor(0, 1);
        // lcd.print(feed_time);
        // lcd.print("/");
        //lcd.print(feed_time);
        //lcd.print(time_to_feed);
        //lcd.print("s");
        lcd.setCursor(15, 1);
        // lcd.print(counter+1);
        if (period == 1) {
          lcd.print(".");
        } else {
          lcd.print(" ");
        }
        redraw = 0;
    }

    delay(100);
    counter = counter + 1;
    if (counter > 10) {
      counter = 0;
      if (period == 0) {
        period = 1;
      } else {
        period = 0;
      }
      time_to_feed_seconds--;
      if (time_to_feed_seconds == 0 && time_to_feed_minutes == 0 && time_to_feed_hours == 0) {
        // Feed here.
        feedFish();
  
        time_to_feed_hours   = wait_time_hours;
        time_to_feed_minutes = wait_time_minutes;
        time_to_feed_seconds = wait_time_seconds;
      }
      
      if (time_to_feed_seconds < 0) {
        time_to_feed_minutes--;
        time_to_feed_seconds = 59;
      }
      if (time_to_feed_minutes < 0) {
        time_to_feed_hours--;
        time_to_feed_minutes = 59;
      }
      if (time_to_feed_hours < 0) {
        // Feed Fish Here!
        time_to_feed_hours = 0;
      }
      redraw = 1;
    }
    
    int button;
    readKey = analogRead(0);
    if (readKey < 790) {
      delay(100);
      readKey = analogRead(0);
    }
    button = evaluateButton(readKey);
    switch (button) {
      case 4:  // This case will execute if the "back" button is pressed
        button = 0;
        activeButton = 1;
        break;
    }
  }
}

/**
 * WAIT TIME 
 */
void menuItem2() { // Sets the wait time.
  int activeButton = 0;
  int redraw = 1;
  int selection = 0;

  while (activeButton == 0) {

    if (redraw == 1) {
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Wait Time: ");
      if (selection == 0) {
        lcd.print(" Hour");
      }
      if (selection == 1) {
        lcd.print(" Mins");
      }
      if (selection == 2) {
        lcd.print(" Secs");
      }
      lcd.setCursor(4, 1);
      if (wait_time_hours < 10) lcd.print("0");
      lcd.print(wait_time_hours);
      lcd.print(":");
      if (wait_time_minutes < 10) lcd.print("0");
      lcd.print(wait_time_minutes);
      lcd.print(":");
      if (wait_time_seconds < 10) lcd.print("0");
      lcd.print(wait_time_seconds);
      
      redraw = 0;
    }
    
    int button;
    readKey = analogRead(0);
    if (readKey < 790) {
      delay(100);
      readKey = analogRead(0);
    }
    button = evaluateButton(readKey);
    switch (button) {
      case 1:  // Right button
        selection++;
        if (selection > 2) selection = 0;
        redraw = 1;
        break;
      case 2:  // Up button
        if (selection == 2) wait_time_seconds++;
        if (wait_time_seconds > 60) {
          wait_time_seconds = 0;
          wait_time_minutes++; 
        }
        if (selection == 1) wait_time_minutes++;
        if (wait_time_minutes > 60) {
          wait_time_minutes = 0;
          wait_time_hours++; 
        }
        if (selection == 0) wait_time_hours++;
        // wait_time_seconds = wait_time_seconds + 100;
        redraw = 1;
        break;
      case 3:  // Down button
        if (selection == 0) wait_time_hours--;
        if (wait_time_hours < 0) wait_time_hours = 0;
        if (selection == 1) wait_time_minutes--;
        if (wait_time_minutes < 0) wait_time_minutes = 0;
        if (selection == 2) wait_time_seconds--;
        if (wait_time_seconds < 0) wait_time_seconds = 0;
        redraw = 1;
        break;
      case 4:  // This case will execute if the "back" button is pressed
        button = 0;
        activeButton = 1;
        break;
    }
  }
}

/**
 * FEED TIME 
 */
void menuItem3() { // Feed Time
  int activeButton = 0;
  int redraw = 1;

  while (activeButton == 0) {

    if (redraw == 1) {
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Feed Time");
      lcd.setCursor(0, 1);
      lcd.print(feed_time);
      redraw = 0;
    }

    int button;
    readKey = analogRead(0);
    if (readKey < 790) {
      delay(100);
      readKey = analogRead(0);
    }
    button = evaluateButton(readKey);
    switch (button) {
      case 2:  // Up button
        feed_time++;
        redraw = 1;
        break;
      case 3:  // Down button
        feed_time--;
        if (feed_time < 0) feed_time = 0;
        redraw = 1;
        break;
      case 4:  // This case will execute if the "back" button is pressed
        button = 0;
        activeButton = 1;
        break;
    }
  }
}

/**
 * MANUAL SERVO 
 */
void menuItem4() { // Function to manually activate the servo.
  int activeButton = 0;
  int redraw = 1;

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Manual Feeding!");

  feedFish();
  
  while (activeButton == 0) {

    if (redraw == 1) {
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Manual Feeding!");
      redraw = 0;
    }
    
    int button;
    readKey = analogRead(0);
    if (readKey < 790) {
      delay(100);
      readKey = analogRead(0);
    }
    button = evaluateButton(readKey);
    switch (button) {
      case 1:  // This case will execute if the "forward" button is pressed
        feedFish();
        redraw = 1;
        //button = 0;
        //activeButton = 1;
        //break;
    }
    switch (button) {
      case 4:  // This case will execute if the "back" button is pressed
        button = 0;
        activeButton = 1;
        break;
    }
  }
}

/**
 * SAVE SETTINGS 
 */
void menuItem5() { // Function executes when you select the 5th item from main menu
  int activeButton = 0;


  EEPROMWriteInt(1, 1252);
  EEPROMWriteInt(3, wait_time_seconds);
  EEPROMWriteInt(5, wait_time_minutes);
  EEPROMWriteInt(7, wait_time_hours);
  EEPROMWriteInt(9, feed_time);

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Settings Saved!");

  while (activeButton == 0) {
    int button;
    readKey = analogRead(0);
    if (readKey < 790) {
      delay(100);
      readKey = analogRead(0);
    }
    button = evaluateButton(readKey);
    switch (button) {
      case 4:  // This case will execute if the "back" button is pressed
        button = 0;
        activeButton = 1;
        break;
    }
  }
}

void menuItem6() { // Function executes when you select the 6th item from main menu
  int activeButton = 0;

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Love You Mom!");

  while (activeButton == 0) {
    int button;
    readKey = analogRead(0);
    if (readKey < 790) {
      delay(100);
      readKey = analogRead(0);
    }
    button = evaluateButton(readKey);
    switch (button) {
      case 4:  // This case will execute if the "back" button is pressed
        button = 0;
        activeButton = 1;
        break;
    }
  }
}

void menuItem7() { // Function executes when you select the 7th item from main menu
  int activeButton = 0;

  lcd.clear();
  lcd.setCursor(3, 0);
  lcd.print("Sub Menu 7");

  while (activeButton == 0) {
    int button;
    readKey = analogRead(0);
    if (readKey < 790) {
      delay(100);
      readKey = analogRead(0);
    }
    button = evaluateButton(readKey);
    switch (button) {
      case 4:  // This case will execute if the "back" button is pressed
        button = 0;
        activeButton = 1;
        break;
    }
  }
}

void menuItem8() { // Function executes when you select the 8th item from main menu
  int activeButton = 0;

  lcd.clear();
  lcd.setCursor(3, 0);
  lcd.print("Sub Menu 8");

  while (activeButton == 0) {
    int button;
    readKey = analogRead(0);
    if (readKey < 790) {
      delay(100);
      readKey = analogRead(0);
    }
    button = evaluateButton(readKey);
    switch (button) {
      case 4:  // This case will execute if the "back" button is pressed
        button = 0;
        activeButton = 1;
        break;
    }
  }
}

void menuItem9() { // Function executes when you select the 9th item from main menu
  int activeButton = 0;

  lcd.clear();
  lcd.setCursor(3, 0);
  lcd.print("Sub Menu 9");

  while (activeButton == 0) {
    int button;
    readKey = analogRead(0);
    if (readKey < 790) {
      delay(100);
      readKey = analogRead(0);
    }
    button = evaluateButton(readKey);
    switch (button) {
      case 4:  // This case will execute if the "back" button is pressed
        button = 0;
        activeButton = 1;
        break;
    }
  }
}

void menuItem10() { // Function executes when you select the 10th item from main menu
  int activeButton = 0;

  lcd.clear();
  lcd.setCursor(3, 0);
  lcd.print("Sub Menu 10");

  while (activeButton == 0) {
    int button;
    readKey = analogRead(0);
    if (readKey < 790) {
      delay(100);
      readKey = analogRead(0);
    }
    button = evaluateButton(readKey);
    switch (button) {
      case 4:  // This case will execute if the "back" button is pressed
        button = 0;
        activeButton = 1;
        break;
    }
  }
}

/**
 * Send the command to turn the servo and feed the fish.
 */
void feedFish() {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Feeding Now!");
  lcd.setCursor(0, 1);
  lcd.print("For ");
  lcd.print(feed_time);
  lcd.print(" secs!");
  // myservo.write(77); // CW SLOW
  myservo.write(76); // CW LESS SLOW
  delay(feed_time * 1000);
  myservo.write(78); // STOP
}

// Ref: http://forum.arduino.cc/index.php?topic=37470.0
void EEPROMWriteInt(int p_address, int p_value)
{
     byte lowByte = ((p_value >> 0) & 0xFF);
     byte highByte = ((p_value >> 8) & 0xFF);

     EEPROM.write(p_address, lowByte);
     EEPROM.write(p_address + 1, highByte);
}

//This function will read a 2 byte integer from the eeprom at the specified address and address + 1
unsigned int EEPROMReadInt(int p_address)
     {
     byte lowByte = EEPROM.read(p_address);
     byte highByte = EEPROM.read(p_address + 1);

     return ((lowByte << 0) & 0xFF) + ((highByte << 8) & 0xFF00);
}
```
  