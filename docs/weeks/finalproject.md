# Codettes bootcamp final project 
## my project is called ANKILA 
For my CB final project  i'm making a portable water turbine 
components needed:
1. diodes 
2. 5v regulator 
3. dc motor 
4. charge circuit

5. esp32(we are using the ttgo t call) for the dashboard and mifi (I have decided to focus on the charge part so this didn't happen anymore)
## electronics 
![Screenshot](../img/fp/fp2.png)
components used:
 - diodes
 - wires 
 - charge circuit 

 the diode bridge : for my diode bridge I soldered 4 diodes to eachother and 4 wires the bridge 
 A diode bridge is an arrangement of four (or more) diodes in a bridge circuit configuration that provides the same polarity of output for either polarity of input. 
 It's mostly used to convert ac input (alternating current) into dc (direct current) output, then it's called a rectifier.
 ![Screenshot](../img/fp/fp8.JPG)
 ![Screenshot](../img/fp/fp9.JPG)
 
 the charge circuit : 
I soldered the common ground to the ground of the chargig port and the plus of the regulator to the plus of the charge port this is needed to power the esp
 
 
 ## Esp 32 
 i'm using is a wrover model.
 
 ![Screenshot](../img/fp/fp1.jpg)
 
 I soldered the headers of the esp 32
 
 
 ![Screenshot](../img/fp/fp6.jpg)
 
 
## arduino ide for esp 32
 I followed [this]( https://randomnerdtutorials.com/installing-the-esp32-board-in-arduino-ide-windows-instructions/) tutorial
 
 ### Voltage & Battery monitoring
 For the voltage &  battery monitoring I had to built this circuit (pangodream picture)
 

 uploading the code
 Problem: 
 1. I didn't solder one of the wires that well so i didn't get any data 
 2. My battery and theirs differs you have to divide it by 3.7
 I know this because I measured the actual voltage.
 
 It works !
 ![Screenshot](../img/fp/fp31.png)
 ## Interface 
 for the interface I turned the esp32 into a accespoint,I will be using websocket for my dashboard 
 what I want to display on my dashboard is :
 
1. the voltage 

2. The battery percentage
 the libraries i I used are:
 - AsyncTCP
 - websocket
 -  pangodream
 - ESPAsyncWebServer
 


 ## Dashboard
 alles van spiffs
 problem:
 Getting my serial monitor data onto my Dashboard
 ![Screenshot](../img/fp/fp3.JPG)
 ![Screenshot](../img/fp/fp5.jpg)
 ![Screenshot](../img/fp/fp4.JPG)
![Screenshot](../img/fp/fp10.png)
 ![Screenshot](../img/fp/fp11.png)
 ![Screenshot](../img/fp/fp12.png)
 ![Screenshot](../img/fp/fp13.png)
 ![Screenshot](../img/fp/fp14.png)
 ![Screenshot](../img/fp/fp15.png)
 ![Screenshot](../img/fp/fp16.png)
 ![Screenshot](../img/fp/fp17.png)
 ![Screenshot](../img/fp/fp18.png)
 ![Screenshot](../img/fp/fp19.png)
 ![Screenshot](../img/fp/fp20.png)
 ![Screenshot](../img/fp/fp21.png)
 ![Screenshot](../img/fp/fp22.png)
 ![Screenshot](../img/fp/fp23.png)
 ![Screenshot](../img/fp/fp24.png)
 ![Screenshot](../img/fp/fp25.png)
 ![Screenshot](../img/fp/fp26.png)

 ## Cad/Cam 

Because of the size of the motor I decided to push the motor in
firstly I made the motor in tinkercad for accuracy 

![Screenshot](../img/fp/fp32.png)

after that I fit the motor in the propeller 

![Screenshot](../img/fp/fp33.png)

then I made it bigger until most of the motor fit inside the propeller because it was to big I had to cut it and we did that by putting a tube over the propeller ad cutting the rest of.

![Screenshot](../img/fp/fp34.png)



 ## references 
 https://www.instructables.com/id/Make-A-Bridge-Rectifier-From-Diodes/
 
 https://www.electronicshub.org/automatic-battery-charger-circuit/
 https://randomnerdtutorials.com/esp32-adc-analog-read-arduino-ide/
 