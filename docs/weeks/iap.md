# Raspberry pi installation   
# WEEK 1
a raspberry pi is a micro processor.
this is what the raspberry pi looks like   

![Screenshot](../img/Iap/download.png)   


 
*  . Download Raspbian version (Jessie or Stretch)

*   Install Disk32Imager

*   Install Putty

*   Install Winscp

*  Install Notepad++

*   Install Angry ip scanner

1.  find your ip adress using command prompt ipconfig       

![Screenshot](../img/Iap/iap1.png)  
  
  
2.   insert sd card in card reader  

3.   put in laptop 

4.   open windiskmanager click the folder icon search for raspian jessie and double click    

![Screenshot](../img/Iap/iap2.png)    


5.   select the sd card name   

![Screenshot](../img/Iap/iap7.png)      

6.   click write  when it's done do not click format

7.   open boot click cmd.txt and then edit with notepad++ you go to the end so till rootwait then space and then ip=192.168.1.165 

![Screenshot](../img/Iap/iap9.png)     

![Screenshot](../img/Iap/iap11.png)    

8.   new file-> new txt document  ssh and remove txt (you do this because you want to use your raspberry pi headless)       


![Screenshot](../img/Iap/iap14.png)      

9. remove the sd card adapter   
10. Connect Pi with pc using cross cable  
11. Power up the Pi
12.  open putty and type in the ip adress of your pi

![Screenshot](../img/Iap/iap19.png)    

13.   if you want to know if your pi is online you go to command prompt and type ping and then your ip adress    
   
![Screenshot](../img/Iap/iap20.png)        
14  Configure Raspbian.Run Rpi setup      
wizard  type  > raspi-config    


![Screenshot](../img/Iap/iap21.png)     

![Screenshot](../img/Iap/iap22.png)         

# WEEK 2
**Basic Linux commands**  
Windows -- Search -- CMD PROMPT   
open putty and winSCP at the same time   
**these are the basic linux commands we will be using**  

*  cd  navigate through directories 
*  cd  .. go back to root directory
*  ls	list 
*  dir	directories
*  mkdir - make directory 
*  touch filename - make text file
*  nano filename.js
*  python  test.py
*  node app.js

node js folder structure/frames   

*  nodejs
      
    *  projects

         *  project1

          *  index.js

          *  public
 
            *  index.html

            *  css

              *  style.css

         *  package.json


![Screenshot](../img/Iap/iap31.png)     


![Screenshot](../img/Iap/iap32.png)       


![Screenshot](../img/Iap/iap33.png)      
 
 
![Screenshot](../img/Iap/iap34.png)    


![Screenshot](../img/Iap/iap35.png)     

you can see this in winSCP too


## Nodejs webserver using express
1.   node --version (to see what version it is) this is the  command to see what version it is app space --version
2.    go to slide 23  curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -  paste this in putty and then let it run 
3.    go to index.js open it and 
4.    go to the project1 folder using cmdline and type npm space init space -y
5.    install nodemon 
6.    integrate npm init -y
7.    you should have your structure in your index.js after you did that you have to put code in it 
8.   pm i express (which means install expressn)
9.    open package.json and change "test..." to " "dev": "nodemon ./index.js"
10.  go to index.js and add all these things that are in the screenshot   

  ```
/**
 * Required External Modules
 */
const express = require("express");
const path = require("path");

/**
 * App Variables
 */
const app = express();
const port = process.env.PORT || "8000";
/**
 *  App Configuration
 */

/**
 * Routes Definitions
 */
/*app.get("/", (req, res) => {
  res.status(200).send("WHATABYTE: Food For Devs");
});*/
app.use(express.static(path.join(__dirname, "public")));
/**
 * Server Activation
 */
app.listen(port, () => {
  console.log(`Listening to requests on http://localhost:${port}`);
});
```  

   ![Screenshot](../img/Iap/iap46.png)   
   

11.  this is what you will see      

  ![Screenshot](../img/Iap/iap48.png)   

12. go back to index.js and comment the whatabyte part out and add  app.use(express.static(path.join(__dirname, "public")));  
13. go to index.html and add the basic html code   
   

14.  go to putty and type in npm run dev   
15.  go to your server and put your ip adress and the port   


  ![Screenshot](../img/Iap/iap50.png)    
  
16.    you can style your website using css and java script


 



## python webserver with flask    
set up a web server and create a simple website using Flask, Python, and HTML/CSS.  

1.  make sure this is all done python 2.7 installed    
pip installed  
sudo pip install flask  
mkdir python   
touch app.py  
2.  create a new folder python and follow the hierachy  
3.  sudo pip install flask  

  ![Screenshot](../img/Iap/iap57.png)    

  ![Screenshot](../img/Iap/iap54.png)         
4.   go to index.html and write this   
5.   <html>
     <body>
    <h1> My website</h1>
    </body>
    </html>   
6. write this in the app.py       

```
from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
```

![Screenshot](../img/Iap/iap56.png) 

7. after you did that this is what the website will look like   
 
![Screenshot](../img/Iap/iap58.png)   


8. you can style your website using css and js  

## Rpi to arduino Serial Communication  

*   build this circuit     
  
![Screenshot](../img/Iap/iap63.png)   

*   open your arduino ide go to file->examples->communinication->physicalpixel  

![Screenshot](../img/Iap/iap60.png)   

*  create a python file called name.py paste the code in the file  

![Screenshot](../img/Iap/iap61.png)    

![Screenshot](../img/Iap/html3.png)  


*  in putty type sudo pip install pyserial
*  connect your arduino to the rpi 
*  type lsub to list your usbports
*  dmesg | grep tty
*  go to your project 2 folder and type sudo pyton index.py   

![Screenshot](../img/Iap/iap62.png)    

 



## week 3 html5 and css3

  1 site map
  2 folderstructure 
  3 research content   
  4 research look and peel 
  5 mock up
  
### day1 and 2
basic html and css   
in the first picture you can see that my page has a different color that was done with css  

![Screenshot](../img/Iap/html1.png)    
Here I changed my text to color too

![Screenshot](../img/Iap/html2.png)  

Here we added articles in this code.

![Screenshot](../img/Iap/html4.png)  

In this picture we are using chrome dev tools to work on our website
![Screenshot](../img/Iap/html5.png)   
Here you can see that the background color is fixed 
![Screenshot](../img/Iap/html6.png)    

 

### Day 3 javascript  

basic java script  

![Screenshot](../img/Iap/html10.png)  

Download chart.js and 
add to your folder  \public\js\lib\
In index.html add code to host chart inside your 

In the second article a chart is added using chart js libraries 
![Screenshot](../img/Iap/html7.png)  

![Screenshot](../img/Iap/html8.png)   




```
<head>
    <script src="js/lib/Chart.js/Chart.js"></script>
    <script src="js/utils.js"></script>
	Add to  Article 2 in your body:
            		<div style="width:100%; height:100%">
				<canvas id="canvas"></canvas>
				<button id="randomizeData">Randomize Data</button>
			<button id="addDataset">Add Dataset</button>
			<button id="removeDataset">Remove Dataset</button>
			<button id="addData">Add Data</button>
			<button id="removeData">Remove Data</button>
		</div>
 Add Javascript at bottom just above the </body>
```

this is what should be put in the javascript tag   

```
<script>
		var MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
		var config = {
			type: 'bar',
			data: {
				labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
				datasets: [{
					label: 'My First dataset',
					backgroundColor: window.chartColors.red,
					borderColor: window.chartColors.red,
					data: [
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor()
					],
					fill: false,
				}, {
					label: 'My Second dataset',
					fill: false,
					backgroundColor: window.chartColors.blue,
					borderColor: window.chartColors.blue,
					data: [
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor()
					],
				}]
			},
			options: {
				responsive: true,
				title: {
					display: false,
					text: 'Chart.js Line Chart'
				},
				tooltips: {
					mode: 'index',
					intersect: false,
				},
				hover: {
					mode: 'nearest',
					intersect: true
				},
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Month'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Value'
						}
					}]
				}
			}
		};

		window.onload = function() {
			var ctx = document.getElementById('canvas').getContext('2d');
			window.myLine = new Chart(ctx, config);
		};

		document.getElementById('randomizeData').addEventListener('click', function() {
			config.data.datasets.forEach(function(dataset) {
				dataset.data = dataset.data.map(function() {
					return randomScalingFactor();
				});

			});

			window.myLine.update();
		});

		var colorNames = Object.keys(window.chartColors);
		document.getElementById('addDataset').addEventListener('click', function() {
			var colorName = colorNames[config.data.datasets.length % colorNames.length];
			var newColor = window.chartColors[colorName];
			var newDataset = {
				label: 'Dataset ' + config.data.datasets.length,
				backgroundColor: newColor,
				borderColor: newColor,
				data: [],
				fill: false
			};

			for (var index = 0; index < config.data.labels.length; ++index) {
				newDataset.data.push(randomScalingFactor());
			}

			config.data.datasets.push(newDataset);
			window.myLine.update();
		});

		document.getElementById('addData').addEventListener('click', function() {
			if (config.data.datasets.length > 0) {
				var month = MONTHS[config.data.labels.length % MONTHS.length];
				config.data.labels.push(month);

				config.data.datasets.forEach(function(dataset) {
					dataset.data.push(randomScalingFactor());
				});

				window.myLine.update();
			}
		});

		document.getElementById('removeDataset').addEventListener('click', function() {
			config.data.datasets.splice(0, 1);
			window.myLine.update();
		});

		document.getElementById('removeData').addEventListener('click', function() {
			config.data.labels.splice(-1, 1); // remove the label first

			config.data.datasets.forEach(function(dataset) {
				dataset.data.pop();
			});

			window.myLine.update();
		});
	</script>
```
 

## Create a chatbox
on putty type in the following command 
 * sudo npm install --save mqtt
 * edit index.js 
 ```
 var mqtt  = require('mqtt');
var client  = mqtt.connect('mqtt://127.0.0.1');

client.on('connect', function () {
client.subscribe('#');
client.publish('/', 'Connected to MQTT-Server');
console.log("\nNodeJS Connected to MQTT-Server\n");
});
// send all messages from MQTT to the Websocket with MQTT topic
client.on('message', function(topic, message){
console.log(topic+'='+message);
io.sockets.emit('mqtt',{'topic':String(topic),payload':String(message)});
});
```
###Interconnect WebSockets with MQTT 2
 **Add the reverse from socket backt to MQTT inside the SocketIO code:** 
 

```
io.sockets.on('connection', function (socket) {
// Add to handle from Socket to MQTT:
socket.on('mqtt', function (data) {
console.log('Receiving for MQTT '+ data.topic + data.payload);
client.publish(data.topic, data.payload);
});
```

 **The app.js code :** 
 ```
var util = require('util');
 var net = require('net');
 var mqtt = require('mqtt');
 var http = require("http"),
    url = require("url"),
    path = require("path"),
    fs = require("fs"),
    port = process.argv[2] || 8888;

var io  = require('socket.io').listen(5000);
var client  = mqtt.connect('mqtt://192.168.1.150');

client.on('connect', function () {
  client.subscribe('/ESP/#');
  client.publish('/ESP/Dashboard/', 'Connected to MQTT-Server');
  console.log("\nDashboard App Connected to MQTT-Server\n");
});

io.sockets.on('connection', function (socket) {
	
	client.on('message', function(topic, message){
	  console.log(topic+'='+message);
	  io.sockets.emit('mqtt',{'topic':String(topic),
		'payload':String(message)});
	});
  socket.on('subscribe', function (data) {
    console.log('Subscribing to '+data.topic);
    socket.join(data.topic);
    client.subscribe(data.topic);
  });
  socket.on('control', function (data) {
    console.log('Receiving for MQTT '+ data.topic + data.payload);
	// TODO sanity check .. is it valid topic ... check ifchannel is "mqtt" and pass message to MQTT server
    client.publish(data.topic, data.payload);
  }); 

});

http.createServer(function(request, response) {

  var uri = url.parse(request.url).pathname
    , filename = path.join(process.cwd(), uri);

  fs.exists(filename, function(exists) {
    if(!exists) {
      response.writeHead(404, {"Content-Type": "text/plain"});
      response.write("404 Not Found\n");
      response.end();
      return;
    }

    if (fs.statSync(filename).isDirectory()) filename += '/index.html';

    fs.readFile(filename, "binary", function(err, file) {
      if(err) {
        response.writeHead(500, {"Content-Type": "text/plain"});
        response.write(err + "\n");
        response.end();
        return;
      }

      response.writeHead(200);
      response.write(file, "binary");
      response.end();
    });
  });
}).listen(parseInt(port, 10));

console.log("Static file server running at\n  => http://localhost:" + port + "/\nCTRL + C to shutdown");
`

 
### socket
* connect 
*subscribe to a topic

`
<script type="text/javascript">
      var socket = io.connect(window.location.hostname + ":5000");
      socket.on('connect', function() {
        socket.emit('subscribe', {
              topic: '/ESP/#'
          });

          socket.on('mqtt', function(msg) {
              var msgTopic = msg.topic.split("/");
              var topic = msgTopic[3];
              var id = msgTopic[2];
              console.log(msg.topic + ' ' + msg.payload);
              //console.log('#'+topic.concat(id));
              // Added for storage of all incoming MQTT messages in Array
              storeMQData(topic.concat(id), msg.payload);
              // end insert
              $('#' + topic.concat(id)).html(msg.payload);
          });
 //$('input.cb-value').prop("checked", true);
          $('.cb-value').click(function() {
              var mainParent = $(this).parent('.toggle-btn');
              if ($(mainParent).find('input.cb-value').is(':checked')) {
                  $(mainParent).addClass('active');
                  socket.emit('control', {
                      'topic': "/ESP/" + $(this).parents('div').attr('id'),
                      'payload': "on"
                  });
              } else {
                  $(mainParent).removeClass('active');
                  socket.emit('control', {
                      'topic': "/ESP/" + $(this).parents('div').attr('id'),
                      'payload': "off"
                  });
                  //io.sockets.emit('mqtt',"/ESP/unit2/pump=off");
                  //socket.emit('closecmd', id);
              }
          });
      });
  </script>
  
<script type="text/javascript">
      var socket = io.connect(window.location.hostname + ":5000");
      socket.on('connect', function() {
        socket.emit('subscribe', {
              topic: '/ESP/#'
          });

          socket.on('mqtt', function(msg) {
              var msgTopic = msg.topic.split("/");
              var topic = msgTopic[3];
              var id = msgTopic[2];
              console.log(msg.topic + ' ' + msg.payload);
              //console.log('#'+topic.concat(id));
              // Added for storage of all incoming MQTT messages in Array
              storeMQData(topic.concat(id), msg.payload);
              // end insert
              $('#' + topic.concat(id)).html(msg.payload);
          });
 //$('input.cb-value').prop("checked", true);
          $('.cb-value').click(function() {
              var mainParent = $(this).parent('.toggle-btn');
              if ($(mainParent).find('input.cb-value').is(':checked')) {
                  $(mainParent).addClass('active');
                  socket.emit('control', {
                      'topic': "/ESP/" + $(this).parents('div').attr('id'),
                      'payload': "on"
                  });
              } else {
                  $(mainParent).removeClass('active');
                  socket.emit('control', {
                      'topic': "/ESP/" + $(this).parents('div').attr('id'),
                      'payload': "off"
                  });
                  //io.sockets.emit('mqtt',"/ESP/unit2/pump=off");
                  //socket.emit('closecmd', id);
              }
          });
      });
  </script>
  ```


![Screenshot](../img/Iap/html11.png)   

![Screenshot](../img/Iap/html12.png)   
 
![Screenshot](../img/Iap/html13.png)   

![Screenshot](../img/Iap/html14.png)   

![Screenshot](../img/Iap/html15.png)   
  

## scada   
 We will be turning leds on and off using arduino, rpi, socket.io, nodejs
 Before we can begin we have to look op the rpi gpio 
 ![Screenshot](../img/Iap/iap64.png)   
 After looking up the gpio you can build the circuit 
 once the circuit is build you have to type these commands on putty : 
 *sudo nodeapp.js
 *sudo apt-get install pigpio
 *npm install pigpio
### The app js code:

```
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
var path = require('path');
// Now setup the local hardware IO
var Gpio = require('pigpio').Gpio;
// start your server
var port=4000;
server.listen(port, (err) => {
  if (err) {
    return console.log('something bad happened', err)
  }

  console.log('server is listening on port 4000')
})

// routing your client app (stored in the /public folder)
app.use(express.static(path.join(__dirname, 'public')));


// Handling Socket messages  as soon as socket becomes active
io.sockets.on('connection', function (socket) {
	// Hookup button behaviour within socket to submit an update when pressed (TO DO)

	// when the client emits 'opencmd', this listens and executes
	socket.on('opencmd', function (data) {
		// Add calls to IO here
		io.sockets.emit('opencmd', socket.username, data);
		setTimeout(function () {
			io.sockets.emit('openvalve', socket.username, data);
			console.log("user: "+socket.username+" opens LED" + data);
			// add some error handling here
			led = new Gpio(parseInt(data), {mode: Gpio.OUTPUT});
			led.digitalWrite(1);
		}, 1000)
	});

	// when the client emits 'closecmd', this listens and executes
	socket.on('closecmd', function (data) {
		// Add calls to IO here
		io.sockets.emit('closecmd', socket.username, data);
		setTimeout(function () {
			io.sockets.emit('closevalve', socket.username, data);
			console.log("user: "+socket.username+" closes LED" + data);
			// add error handling here
			led = new Gpio(parseInt(data), {mode: Gpio.OUTPUT});
			led.digitalWrite(0);
		}, 1000)
		
/* 		setTimeout(function () {
			led.digitalWrite(0);
			io.sockets.emit('closevalve', socket.username, data);
		}, 1000) */
	});

});
```
### The index.html code 
```
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<meta http-equiv="X-UA-Compatible" content="IE=9" />

<head>
    <title>Codettes Tutorial</title>
    <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
    <link type="text/css" rel="stylesheet" href="css/app.css" media="all">
</head>
<script src="js/socket.js"></script>
<script src="js/jquery.js"></script>
<script>
    var socket = io.connect(window.location.hostname + ":4000"); //'http://192.168.1.163:4000'); //set this to the ip address of your node.js server

    // listener, whenever the server emits 'openvalve', this updates the username list
    socket.on('opencmd', function(username, data) {
        $('#' + data + ' > div.feedback > div.circle.status > div.circle.command').removeClass('red').addClass('green');
    });
    socket.on('openvalve', function(username, data) {
        $('#' + data + ' > div.feedback > div.circle.status').removeClass('red').addClass('green');
    });


    // listener, whenever the server emits 'openvalve', this updates the username list
     socket.on('closecmd', function(username, data) {
        $('#' + data + ' > div.feedback > div.circle.status > div.circle.command').removeClass('green').addClass('red');
    });
	socket.on('closevalve', function(username, data) {
        $('#' + data + ' > div.feedback > div.circle.status').removeClass('green').addClass('red');
    });

    // on load of page
    $(function() {

        // when the client clicks OPEN
        $('.open').click(function() {
            var id = $(this).parent().attr("id");;
            console.log("user clicked open : " + id);
            socket.emit('opencmd', id);
        });

        // when the client clicks CLOSE
        $('.close').click(function() {
            var id = $(this).parent().attr("id");;
            console.log("user clicked on close : " + id);
            socket.emit('closecmd', id);
        });

    });
</script>

<body>
    <p>

        <div id="17" class="valve">
            <h3>Control 1</h3>
            <div class="open">OPEN</div>
            <div class="close">CLOSE</div>
            <div class="clear"></div>
            <div class="feedback">
                <div class="circle status green">
                    <div class="circle command red"></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div id="27" class="valve">
            <h3>Control 2</h3>
            <div class="open">OPEN</div>
            <div class="close">CLOSE</div>
            <div class="clear"></div>
            <div class="feedback">
                <div class="circle status green">
                    <div class="circle command red"></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div id="18" class="valve">
            <h3>Control 3</h3>
            <div class="open">OPEN</div>
            <div class="close">CLOSE</div>
            <div class="clear"></div>
            <div class="feedback">
                <div class="circle status green">
                    <div class="circle command red"></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div id="23" class="valve">
            <h3>Control 4</h3>
            <div class="open">OPEN</div>
            <div class="close">CLOSE</div>
            <div class="clear"></div>
            <div class="feedback">
                <div class="circle status green">
                    <div class="circle command red"></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
</body>
```

![Screenshot](../img/Iap/html16.png)   






