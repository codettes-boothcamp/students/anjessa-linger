* videoprodution  

![Screenshot](../img/Screenshot__8_.jpg) 

create a new project = new -> project

![Screenshot](../img/Screenshot__28_.jpg)  
Name, Capture-> HDV, docs -> Default.  

![Screenshot](../img/Screenshot__10_.jpg)   

WorkSpace → Project / Timeline / Effects
Drag video (left )to timeline (right


![Screenshot](../img/Screenshot__14_.jpg)  

![Screenshot](../img/Screenshot__16_.jpg)  
effects control.

![Screenshot](../img/Screenshot__18_.jpg)  
Effects → Ultra Key
Effects Controls → Key Color to remove background

![Screenshot](../img/Screenshot__20_.jpg)  
Import → Background Image
Drag background image to timeline between video and audio

![Screenshot](../img/Screenshot__21_.jpg)  
If background small
Right click → Scale to Frame Size

![Screenshot](../img/Screenshot__23_.jpg)  
If background small
Effects Control → Scale 120 or more

![Screenshot](../img/Screenshot__24_.jpg)  
Adding Titles
New → Legacy Title

![Screenshot](../img/Screenshot__25_.jpg)  
Adding Titles
Select Tool → Text 
Font Family georgia

![Screenshot](../img/Screenshot__27_.jpg)  
Adding Titles
Drag title to timeline 
Place / Stretch as pleased


![Screenshot](../img/Screenshot__29_.jpg)  
Exporting
Export → Media



![Screenshot](../img/Screenshot__3_.jpg)  
Exporting
Preset → Youtube 720p HD

![Screenshot](../img/Screenshot__4_.jpg)  
Exporting
Output → Select Path

![Screenshot](../img/Screenshot__6_.jpg)  
Exporting
Scroll to bottom : Export 
(default path : Output)
